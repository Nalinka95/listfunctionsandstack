package com.company;

import java.util.*;

public class Main {
    ArrayList<Integer> numbers=new ArrayList<>();
    public static void main(String[] args) {
        CheckBalancedParentheses pare= new CheckBalancedParentheses();
        boolean isBalanced=pare.checkBalance("{{}}");
        System.out.println("is it balanced : "+isBalanced);

    }
    public ArrayList<Integer> checkDuplicateAndAdd(Integer number){
        if(numbers==null){
            numbers.add(number);
            return numbers;
        }
        else{
            for(Integer element: numbers){
                if(element==number){
                    return numbers;
                }
            }
            numbers.add(number);
            return numbers;
        }

    }
    public ArrayList<Integer> removeDuplicateFromArray(ArrayList<Integer> inputList){
        ArrayList<Integer> outputs= new ArrayList<Integer>();
        for(Integer input: inputList ){
            boolean numberIsInTheList=false;
            for(Integer output: outputs){
                if(output==input){
                    numberIsInTheList=true;
                }
            }
            if(!numberIsInTheList){
                outputs.add(input);
            }
        }
        return outputs;
    }
    public LinkedList<Integer> merge(LinkedList<Integer> a, LinkedList<Integer> b){
        a.addAll(b);
        Collections.sort(a);
        return a;

    }
    public void printList(LinkedList<Integer> print){
        for(Integer printInt: print){
            System.out.print(printInt+" ");
        }

    }
    public LinkedList<Integer> sortHashMap(LinkedList<Integer> inputList){
        LinkedHashMap<Integer,Integer> inputMap= new LinkedHashMap<Integer, Integer>();
        for(Integer element: inputList){
            if(inputMap.get(element)==null){
                inputMap.put(element,1);
            }
            else{
                int newValue= inputMap.get(element)+1;
                inputMap.replace(element,newValue);
            }
        }
        LinkedList<Integer> sortedList=new LinkedList<Integer>();
        for(Map.Entry<Integer,Integer> entry:inputMap.entrySet()){
            if(entry.getValue()==1){
                sortedList.add(entry.getKey());
            }
            else{
                for(int index=0;index<entry.getValue();index++){
                    sortedList.addFirst(entry.getKey());
                }
            }
        }
    return sortedList;
    }

}
