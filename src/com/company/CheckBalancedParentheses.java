package com.company;

import java.util.Stack;

public class CheckBalancedParentheses {
    Stack parentheses = new Stack();
    private String parenthesesString;

    public CheckBalancedParentheses(String parenthesesString) {
        this.parenthesesString = parenthesesString;
        Stack parentheses = new Stack();
    }

    public CheckBalancedParentheses() {
        Stack parentheses = new Stack();
    }

    public boolean checkBalance(String stringParentheses) {
        boolean isBalanced=true;
        int size= stringParentheses.length();
        for(int index=0;index<size;index++){
            char charValueOfCurrentString= stringParentheses.charAt(index);
            if(charValueOfCurrentString=='('||charValueOfCurrentString=='{'||charValueOfCurrentString=='['){
                parentheses.push(charValueOfCurrentString);
            }
            else {
                char lastInsertedCharValue=(char)parentheses.pop();
                isBalanced= isMatchingPair(lastInsertedCharValue,charValueOfCurrentString);
                if(!isBalanced){
                    return false;
                }
            }
        }
        if(parentheses.isEmpty()){
            return true;
        }
        else {
            return false;
        }
    }

    boolean isMatchingPair(char stackElementOne, char stackElementTwo) {
        if (stackElementOne == '(' && stackElementTwo == ')')
            return true;
        else if (stackElementOne == '{' && stackElementTwo == '}')
            return true;
        else if (stackElementOne == '[' && stackElementTwo == ']')
            return true;
        else
            return false;

    }

}
